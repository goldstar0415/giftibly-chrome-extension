
GIFTIBLYSTYLE = {
    loadCSS: function(){
        var style = document.createElement('link');
        style.rel = "stylesheet";
        style.type = "text/css";
        style.href = chrome.extension.getURL('main.css');
        (document.head || document.documentElement).appendChild(style);
    }
};

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (request.type == "addCSS"){
        GIFTIBLYSTYLE.loadCSS();
    }
});
